Rails.application.routes.draw do
  get 'home/index'

  devise_for :users


  # devise_for :users,
  #            path: '',
  #            path_names: {
  #              sign_in: 'login',
  #              sign_out: 'logout',
  #              registration: 'signup'
  #            },
  #            controllers: {
  #              sessions: 'sessions',
  #              registrations: 'registrations'
  #            }

 	root "home#index"
             
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
